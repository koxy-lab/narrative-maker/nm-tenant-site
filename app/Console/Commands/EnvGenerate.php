<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class EnvGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'env:generate {--name=} {--domain=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new env file for narrative';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $project_path =  dirname(dirname(dirname(dirname(__FILE__))));

        $name = $this->option('name');
        $domain = $this->option('domain');

        $path = $project_path . "/env/{$domain}";

        $file = fopen($path, "w");

        fwrite($file, $this->template($project_path . '/env/template.php',
            [
                'name' => $name,
                'domain' => $domain,
            ]
        ));

        fclose($file);

        $this->line( $domain . '\'s updated');

        return null;
    }

    /**
     * Simple Templating function
     *
     * @param $file   - Path to the PHP file that acts as a template.
     * @param $args   - Associative array of variables to pass to the template file.
     * @return string - Output of the template file. Likely HTML.
     */
    function template( $file, $args ){
        // ensure the file exists
        if ( !file_exists( $file ) ) {
            return '';
        }

        // Make values in the associative array easier to access by extracting them
        if ( is_array( $args ) ){
            extract( $args );
        }

        // buffer the output (including the file is "output")
        ob_start();
        include $file;
        return ob_get_clean();
    }
}
