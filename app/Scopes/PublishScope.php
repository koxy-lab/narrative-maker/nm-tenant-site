<?php
/**
 * Created by PhpStorm.
 * User: Litu
 * Date: 23/11/2018
 * Time: 1:55
 */

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PublishScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereNotNull('published_at')
                ->where('published_at', '<', new \DateTime())
                ->latest('published_at');
    }
}
