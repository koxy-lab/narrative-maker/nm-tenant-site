<?php

namespace App\Providers;

use App\Models\Tenant\Chapter;
use App\Models\Tenant\Extra;
use App\Models\Tenant\Page;
use App\Models\Tenant\Project;
use App\Models\Tenant\Sheet;
use App\Models\Tenant\Taxonomy;
use App\Models\Tenant\Webnovel;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::bind('page', function($value){
            return Page::where('slug', $value)->orWhere(function ($query) use ($value){
                if (is_numeric($value)){
                    $query->where('id', $value);
                }
            })->firstOrFail();
        });

        Route::bind('project', function($value){
            return Project::where('slug', $value)->orWhere(function ($query) use ($value){
                if (is_numeric($value)){
                    $query->where('id', $value);
                }
            })->firstOrFail();
        });

        Route::bind('extra', function($value){
            return Extra::where('slug', $value)->orWhere(function ($query) use ($value){
                if (is_numeric($value)){
                    $query->where('id', $value);
                }
            })->firstOrFail();
        });

        Route::bind('taxonomy', function($value){
            return Taxonomy::where('slug', $value)->orWhere(function ($query) use ($value){
                if (is_numeric($value)){
                    $query->where('id', $value);
                }
            })->firstOrFail();
        });

        Route::bind('sheet', function($value){
            return Sheet::where('slug', $value)->orWhere(function ($query) use ($value){
                if (is_numeric($value)){
                    $query->where('id', $value);
                }
            })->firstOrFail();
        });

        Route::bind('webnovel', function($value){
            return Webnovel::where('slug', $value)->orWhere(function ($query) use ($value){
                if (is_numeric($value)){
                    $query->where('id', $value);
                }
            })->firstOrFail();
        });

        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapDashboardRoutes();

        //

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    protected function mapDashboardRoutes()
    {
        Route::middleware('web','verified')
            ->name('dashboard.')
            ->prefix('dashboard')
            ->namespace($this->namespace . '\Dashboard')
             ->group(base_path('routes/dashboard.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
