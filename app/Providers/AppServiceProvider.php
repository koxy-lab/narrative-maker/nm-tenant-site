<?php

namespace App\Providers;

use App\Http\ViewComposers\TenantSite\SiteViewComposer;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Laravel\Telescope\TelescopeServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', SiteViewComposer::class);

        /**
         *  Check if the app run in debug mode
         */
        Blade::if('debug', function () {
            return config('app.debug');
        });

        /**
         *  Check if the user have a subscription
         */
        Blade::if('subscribed', function ($plan = null) {

            // If user auth
            if (auth()) {

                // If plan not null and need to validate a subscription
                if ($plan){

                    // get agreement
                    $agree = auth()->user()->agreements()->where('paypal_state', 'Active')->first();

                    // if agreement exist
                    if($agree){
                        $count = $agree->plan()->where('name', $plan)->count();
                        return $count;
                    }
                    else{
                        return 0;
                    }
                }
                // If need to validate if the user have a subscription
                else {
                    $data = auth()->user()->agreements()->where('paypal_state', 'Active')->count();
                    return $data;
                }
            }
            return null;
        });

        /**
         *  aaa
         */
        Blade::directive('plan', function ($name) {
            // check if the user is authenticated
            if (auth()) {
                // check if the user have a plan with that name
                $agree = auth()->user()->agreements()->where('paypal_state', 'Active')->first();

                return $agree->plan()->where('name', $name)->count();
            }
            return null;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
