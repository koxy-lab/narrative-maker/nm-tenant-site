<?php

namespace App\Http\Middleware\TenantSite;

use App\Models\Narrative;
use App\Events\Tenant\TenantIdentified;
use Closure;

class SetTenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenant = $this->resolveTenant(session('tenant'));

        if (!$tenant) {
            return $next($request);
        }

        event(new TenantIdentified($tenant));

        return $next($request);
    }

    protected function resolveTenant($uuid)
    {
        return Narrative::where('uuid', $uuid)->first();
    }
}
