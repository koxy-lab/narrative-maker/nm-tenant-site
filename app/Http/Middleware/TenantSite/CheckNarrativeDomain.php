<?php

namespace App\Http\Middleware\TenantSite;

use App\Events\Tenant\TenantIdentified;
use App\Models\Narrative;
use App\Tenant\Database\DatabaseManager;
use Closure;

class CheckNarrativeDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // todo: verify if active narrative

        // Get the host
        $domain = $request->getHost();

        // Parse host
        $parsedHost = explode('.', $domain, 2);

        // if the domain have a www as subdomain
        if ( $parsedHost[0] == 'www' ){
            $this->getByDomain( $request, $parsedHost[1] );
        }
        else if ( $parsedHost[0] != 'www' && $parsedHost[1] == config('app.domain') ){
            $this->getBySlug( $request, $parsedHost );
        }
        else {
            $this->getByDomain( $request, $domain );
        }

        return $next($request);
    }

    protected function setNarrativeTenant($narrative, $request){
        if (!$narrative) {
            // Redirect to the web app
            if( config('app.debug') ){
                return redirect('nm-app.test');
            } else {
                return redirect('narrativemaker.com');
            }
        } else {
            $uuid = $narrative->uuid;
            session()->put('tenant', $uuid);

            $request->attributes->add(['tenant' => $narrative]);

            $tenant = $narrative; //$this->resolveTenant(session('tenant'));

            event(new TenantIdentified($tenant));
        }

        return null;
    }

    /**
     * Get narrative by domain
     *
     * @param $url
     *
     * @return mixed
     */
    protected function getNarrativeTenant($url){
        return \App\Models\Narrative::where('domain', $url)->first();
    }

    /**
     * Get narrative by name
     *
     * @param $name
     *
     * @return mixed
     */
    protected function getNarrativeTenantByName($name){
        return \App\Models\Narrative::where('slug', str_slug($name))->first();
    }

    /**
     * @param $request
     * @param Closure $next
     * @param $domain
     */
    protected function getByDomain( $request, $domain ): void {
        $narrative = $this->getNarrativeTenant( $domain );
        $this->setNarrativeTenant( $narrative, $request );
    }

    /**
     * @param $request
     * @param Closure $next
     * @param $parsedHost
     */
    protected function getBySlug( $request, $parsedHost ): void {
        $narrative = $this->getNarrativeTenantByName( $parsedHost[0] );
        $this->setNarrativeTenant( $narrative, $request );
    }
}
