<?php
/**
 * Created by PhpStorm.
 * User: Litu
 * Date: 14/11/2018
 * Time: 17:10
 */

namespace App\Http\Controllers\Auth;


trait RedirectTo {

    /**
     * Where to redirect users after registration.
     *
     * @void string
     */
    protected function redirectTo()
    {
        return route('dashboard.home');
    }

}
