<?php

namespace App\Http\Controllers;
;

use App\Models\Tenant\Extra;
use App\Models\Tenant\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExtraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $extras = Extra::get();

        return view($this->template.'.extras.index', compact('extras'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tenant\Extra  $extra
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Extra $extra)
    {
        return view($this->template.'.extras.show', compact('extra'));
    }
}
