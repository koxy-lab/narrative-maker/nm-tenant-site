<?php

namespace App\Http\Controllers;

use App\Models\Tenant\Chapter;
use App\Models\Tenant\Extra;
use App\Models\Tenant\Page;
use App\Models\Tenant\Project;
use App\Models\Tenant\Webnovel;
use App\Tenant\Manager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index()
    {
        $projects = Project::limit(3)->latest()->get();
        $extras = Extra::limit(3)->latest()->get();
        $webnovels = Webnovel::limit(3)->latest()->get();
        $chapters = Chapter::limit(3)->latest()->get();

        return view($this->template.'.home')
            ->withProjects($projects)
            ->withExtras($extras)
            ->withWebnovels($webnovels)
            ->withChapters($chapters);
    }

    public function getPage(Page $page)
    {
        return view($this->template.'.page', compact('page'));
    }
}
