<?php

namespace App\Http\Controllers;
;

use App\Models\Tenant\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Project::get();

        return view($this->template.'.projects.index')->with('projects', $pages);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tenant\Project  $project
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return view($this->template.'.projects.show', compact('project'));
    }
}
