<?php

namespace App\Http\Controllers;

use App\Models\Tenant\Sheet;
use App\Models\Tenant\Taxonomy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WikiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taxonomies = Taxonomy::whereNull('taxonomy_id')->get();

        return view($this->template.'.wiki.index')->with('taxonomies', $taxonomies);
    }

    /**
     * Display the specified resource.
     *
     * @param $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $taxonomy = Taxonomy::where('slug', $slug)->first();
        $sheet = Sheet::where('slug', $slug)->first();

        if (Taxonomy::where('slug', $slug)->first()){
            return view($this->template.'.wiki.taxonomy', compact('taxonomy'));
        }
        if (Sheet::where('slug', $slug)->first()){
            return view($this->template.'.wiki.show', compact('sheet'));
        }
        else {
            return abort(404);
        }
    }
}
