<?php

namespace App\Http\Controllers;

use App\Models\Tenant\Chapter;
use App\Models\Tenant\Webnovel;
use Illuminate\Http\Request;

class ChapterController extends Controller
{
    public function show($webnovel, $chapter)
    {
        $data = $webnovel->chapters()->where('slug', $chapter)->firstOrFail();

        return view($this->template.'.webnovels.chapters.show')->withChapter($data);
    }
}
