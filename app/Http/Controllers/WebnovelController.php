<?php

namespace App\Http\Controllers;

use App\Models\Tenant\Webnovel;
use Illuminate\Http\Request;

class WebnovelController extends Controller
{
    public function index()
    {
        $webnovels = Webnovel::limit(3)->get();

        return view($this->template.'.webnovels.index')
            ->withWebnovels($webnovels);
    }

    public function show(Webnovel $webnovel)
    {
        return view($this->template.'.webnovels.show', compact('webnovel'));
    }
}
