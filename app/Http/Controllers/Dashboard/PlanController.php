<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Tenant\Plan;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    public function index() {
        $plans = Plan::where('paypal_state', 'ACTIVE')->get();

        return view('dashboard.plans', compact('plans'));
    }
}
