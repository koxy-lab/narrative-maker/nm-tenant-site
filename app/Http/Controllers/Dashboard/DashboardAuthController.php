<?php
/**
 * Created by PhpStorm.
 * User: Litu
 * Date: 14/11/2018
 * Time: 17:13
 */

namespace App\Http\Controllers\Dashboard;


use App\Http\Controllers\Controller;

class DashboardAuthController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

}
