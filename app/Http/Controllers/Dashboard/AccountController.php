<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AccountController extends DashboardAuthController
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.account');
    }

    public function update(Request $request)
    {
        $id = auth()->user()->id;

        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'alpha_dash', 'min:6', 'max:255', Rule::unique('users')->ignore($id)],
            'email' => ['sometimes', 'required', 'email', 'unique:users'. ',id,' . $id],
        ];

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('dashboard/account/')
                           ->withErrors($validator)
                           ->withInput(Input::except('password'));
        } else {
            $user = User::find($id);


            $user->name = $request->name;
            $user->username = $request->username;
            $user->email = $request->email;

            $user->update();

//            dd($user);
            // redirect
            Session::flash('message', 'Successfully updated nerd!');
            return redirect()->route('dashboard.account');
        }
    }
}
