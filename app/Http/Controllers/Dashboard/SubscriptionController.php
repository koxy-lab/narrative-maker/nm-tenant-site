<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Narrative;
use App\Models\Tenant\Plan;
use App\Paypal\PaypalAgreements;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    public function subscribe( Request $request ) {

        $plan = Plan::where('paypal_id', $request->plan_id)->first();
        $tenant = Narrative::where('uuid', session('tenant'))->first();

        if($plan) {

            $name = $plan->name . ' Agreement subscription';
            $description = 'Agreement subscription for ' . $tenant->name;

            $agreement = new PaypalAgreements($name, $description);
            return $agreement->create($plan->paypal_id);

        } else {
            back();
        }
    }

    public function unsubscribe( Request $request ) {
        $planId = $request->plan_id;



        return redirect(route('dashboard.home'));
    }
}
