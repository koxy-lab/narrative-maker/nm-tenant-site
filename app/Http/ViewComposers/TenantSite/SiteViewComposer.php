<?php

namespace App\Http\ViewComposers\TenantSite;

use App\Models\Narrative;
use Illuminate\View\View;

class SiteViewComposer
{
    public function compose(View $view)
    {
        $narrative = Narrative::where('uuid', session('tenant'))->first();
        $view->with('tenant', $narrative);
    }
}
