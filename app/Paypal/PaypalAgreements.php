<?php
/**
 * Created by PhpStorm.
 * User: Litu
 * Date: 30/11/2018
 * Time: 13:47
 */

namespace App\Paypal;

use Carbon\Carbon;
use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;

class PaypalAgreements extends Paypal
{
    protected $name;
    protected $description;

    public function __construct($name, $description)
    {
        parent::__construct();

        $this->name = $name;
        $this->description = $description;
    }

    public function create( $id )
    {
        $now = Carbon::now();
        $now = $now->toIso8601ZuluString();

        $plan = new Plan();
        $plan->setId($id);

        $agreement = new Agreement();
        $agreement->setName($this->name)
                  ->setDescription($this->description)
                  ->setStartDate('2019-06-17T9:45:04Z'); // '2019-06-17T9:45:04Z'

        $agreement->setPlan($plan);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);

//        $shippingAddress = new ShippingAddress();
//        $shippingAddress->setLine1('111 First Street')
//                        ->setCity('Saratoga')
//                        ->setState('CA')
//                        ->setPostalCode('95070')
//                        ->setCountryCode('US');
//        $agreement->setShippingAddress($shippingAddress);

        $agreement = $agreement->create($this->apiContext);

        $approvalUrl = $agreement->getApprovalLink();

        return redirect()->to($approvalUrl);
    }


    public function accept( $id )
    {
        //
    }

    public function cancel( $id )
    {
        //
    }
}
