<?php
/**
 * Created by PhpStorm.
 * User: Litu
 * Date: 27/11/2018
 * Time: 12:02
 */

namespace App\Paypal;


use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class PayPal {

    protected $apiContext;
    protected $oAuthCredential;

    public function __construct() {
        $this->oAuthCredential = new OAuthTokenCredential( config('services.paypal.id'), config('services.paypal.secret') );
        $this->apiContext = new ApiContext($this->oAuthCredential);
    }

}
