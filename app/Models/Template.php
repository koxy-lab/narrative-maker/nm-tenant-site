<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{

    use nmConecction;

    public function narratives()
    {
        return $this->hasMany(Narrative::class);
    }
}
