<?php

namespace App\Models;

use App\Tenant\Models\Tenant;
use App\Tenant\Traits\ForSystem;
use App\Tenant\Traits\IsTenant;
use App\Models\TenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Narrative extends Model implements Tenant
{
    use SoftDeletes, IsTenant, ForSystem;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'uuid',
        'domain',
        'description',
        'text'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'domain',
    ];

    /**
     * One to one Relations
     */

    public function tenantConnection(){
        return $this->hasOne(TenantConnection::class, 'narrative_id', 'id');
    }

    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    /**
     * Many to many Relations
     */

    public function users(){
        return $this->belongsToMany(User::class);
    }
}
