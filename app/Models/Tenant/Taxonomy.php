<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Sheet;
use App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Taxonomy extends Model
{
    use ForTenants, SoftDeletes;

    protected $fillable = [
        'name'
    ];

    public function father(){
        return $this->hasOne(Taxonomy::class);
    }


    public function children(){
        return $this->hasMany(Taxonomy::class);
    }

    public function sheets(){
        return $this->belongsToMany(Sheet::class);
    }
}
