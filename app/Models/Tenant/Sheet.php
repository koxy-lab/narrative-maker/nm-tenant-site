<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Taxonomy;
use App\Scopes\PublishScope;
use App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sheet extends Model
{
    use ForTenants, SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PublishScope());
    }

    protected $fillable = [
        'title', 'description'
    ];

    public function taxonomies(){
        return $this->belongsToMany(Taxonomy::class);
    }
}
