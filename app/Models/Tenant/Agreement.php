<?php

namespace App\Models\Tenant;

use App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    use ForTenants;

    public function plan() {
        return $this->belongsTo(Plan::class);
    }

    public function users() {
        return $this->belongsTo(User::class);
    }
}
