<?php

namespace App\Models\Tenant;

use App\Models\Tenant\File;
use App\Scopes\PublishScope;
use App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use ForTenants, SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PublishScope());
    }

    protected $fillable = [
        'name'
    ];

    public function files()
    {
        return $this->hasMany(File::class);
    }
}
