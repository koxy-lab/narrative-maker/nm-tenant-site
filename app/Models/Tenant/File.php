<?php

namespace App\Models\Tenant;

use App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use ForTenants, SoftDeletes;

    protected $fillable = [
        'name', 'path'
    ];
}
