<?php

namespace App\Models\Tenant;

use App\Models\HasPrice;
use App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
    use ForTenants, SoftDeletes, HasPrice;

    protected $fillable = [
        'name', 'description', 'price', 'paypal_state', 'paypal_id'
    ];

    public function agreements() {
        return $this->hasMany(Agreement::class);
    }
}
