<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Chapter;
use App\Scopes\PublishScope;
use App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Webnovel extends Model
{
    use ForTenants, SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PublishScope());
    }

    public function chapters()
    {
        return $this->hasMany(Chapter::class);
    }
}
