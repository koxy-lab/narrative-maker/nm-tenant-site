<?php
/**
 * Created by PhpStorm.
 * User: Litu
 * Date: 29/11/2018
 * Time: 14:06
 */

namespace App\Models;


use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;

trait HasPrice {

//    public function getPriceAtrribute( $value ) {
//        return new Money($value, new Currency('USD'));
//    }

    public function getFormattedPriceAttribute() {
        $money = new Money($this->price, new Currency('USD'));
        $currencies = new ISOCurrencies();

        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        echo $moneyFormatter->format($money);
    }
    public function getCompletePriceAttribute() {
        $money = new Money($this->price, new Currency('USD'));
        $currencies = new ISOCurrencies();

        $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::CURRENCY);
        $moneyFormatter = new IntlMoneyFormatter($numberFormatter, $currencies);

//        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        echo $moneyFormatter->format($money);
    }
}
