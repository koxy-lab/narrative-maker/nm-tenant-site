<?php

namespace App\Tenant\Database;

use App\Tenant\Models\Tenant;
use Illuminate\Support\Facades\DB;

class DatabaseCreator
{
    public function create(Tenant $tenant)
    {
        $prefix = config('tenant.prefix');

        return DB::statement("
            CREATE DATABASE {$prefix}{$tenant->id}
        ");
    }
}
