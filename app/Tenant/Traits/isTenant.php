<?php
/**
 * Created by PhpStorm.
 * User: Litu
 * Date: 18/09/2018
 * Time: 16:31
 */

namespace App\Tenant\Traits;

use App\Models\TenantConnection;
use App\Tenant\Models\Tenant;
use Webpatser\Uuid\Uuid;

trait IsTenant
{

    public static function boot()
    {
        parent::boot();

        static::creating(function ($tenant) {
            $tenant->uuid = Uuid::generate(4);
        });

        static::created(function ($tenant) {
            $tenant->tenantConnection()->save(static::newDatabaseConnection($tenant));
        });
    }

    protected static function newDatabaseConnection(Tenant $tenant)
    {
        return new TenantConnection([
            'database' => 'nm_narrative_' . $tenant->id
        ]);
    }

    public function tenantConnection()
    {
        return $this->hasOne(TenantConnection::class, 'narrative_id', 'id');
    }
}
