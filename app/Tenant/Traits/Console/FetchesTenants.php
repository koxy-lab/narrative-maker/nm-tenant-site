<?php

namespace App\Tenant\Traits\Console;

use App\Models\Narrative;

trait FetchesTenants
{
    public function tenants($ids = null)
    {
        $tenants = Narrative::query();

        if ($ids) {
            $tenants = $tenants->whereIn('id', $ids);
        }

        return $tenants;
    }
}
