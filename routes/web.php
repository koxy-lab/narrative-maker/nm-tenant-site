<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');

Auth::routes(['verify' => true]);

//
Route::get('/proyectos/', 'ProjectController@index')->name('projects.index');
Route::get('/proyectos/{project}', 'ProjectController@show')->name('projects.show');

//
Route::get('/extras/', 'ExtraController@index')->name('extras.index');
Route::get('/extras/{extra}', 'ExtraController@show')->name('extras.show');

Route::get('/wiki/', 'WikiController@index')->name('wiki.index');
Route::get('/wiki/{slug}', 'WikiController@show')->name('wiki.show');

Route::get('/webnovels/', 'WebnovelController@index')->name('webnovels.index');
Route::get('/webnovels/{webnovel}', 'WebnovelController@show')->name('webnovels.show');
Route::get('/webnovels/{webnovel}/{chapter}', 'ChapterController@show')->name('webnovels.chapters.show');

Route::get('/{page}', 'PageController@getPage');
