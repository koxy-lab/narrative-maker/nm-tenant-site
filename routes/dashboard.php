<?php
/**
 * Created by PhpStorm.
 * User: Litu
 * Date: 23/11/2018
 * Time: 3:25
 */


Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/account', 'AccountController@index')->name('account');
Route::put('/account', 'AccountController@update')->name('account');

Route::get('/plans', 'PlanController@index')->name('plans.index');

Route::post('/subscriptions', 'SubscriptionController@subscribe')->name('subscriptions.subscribe');
Route::delete('/subscriptions', 'SubscriptionController@unsubscribe')->name('subscriptions.unsubscribe');
