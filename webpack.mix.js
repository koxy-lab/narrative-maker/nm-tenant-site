"use strict";

const mix = require('laravel-mix');

const templates = ['default','ancient-vanilla','dark-soul'];

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/dashboard/app.js', 'public/dashboard/js')
    .sass('resources/sass/dashboard/styles.scss', 'public/dashboard/css');

// mix.js('resources/js/templates/default/app.js', 'public/templates/default/js')
//     .sass('resources/sass/templates/default/styles.scss', 'public/templates/default/css');
//
// mix.js('resources/js/templates/ancient-vanilla/app.js', 'public/templates/ancient-vanilla/js')
//     .sass('resources/sass/templates/ancient-vanilla/styles.scss', 'public/templates/ancient-vanilla/css');
//
// mix.js('resources/js/templates/ancient-vanilla/app.js', 'public/templates/ancient-vanilla/js')
//     .sass('resources/sass/templates/ancient-vanilla/styles.scss', 'public/templates/ancient-vanilla/css');

templates.forEach(template => {
    mix.js('resources/js/templates/' + template + '/app.js', 'public/templates/' + template + '/js')
        .sass('resources/sass/templates/' + template + '/styles.scss', 'public/templates/' + template + '/css');
});
