@if(false)
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="Título de página">
    <meta name="twitter:description" content="Descripción de página, no menos de 200 caracteres">
    <meta name="twitter:creator" content="@author_handle">
    <-- Las imágenes de sumario de Twitter deben ser de al menos 200x200px -->
    <meta name="twitter:image" content=" http://www.ejemplo.com/imagen.jpg">
@endif

<!-- Open Graph data -->
<meta property="og:title" content="{{ $title }}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{ Request::url() }}" />

@isset($image)
    <meta property="og:image" content="{{ $image }}" />
    {{--@else--}}
    {{--<meta property="og:image" content="{{ asset() }}" />--}}
@endisset

<meta property="og:description" content="{{ $description }}" />
<meta property="og:site_name" content="{{ $tenant->name }}" />

{{--Facebook numeric ID--}}
@isset($faceID)
    <meta property="fb:admins" content="{{ $faceID }}" />
@endisset
