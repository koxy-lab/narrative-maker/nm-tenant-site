<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $tenant->name ?? config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('templates/' . $tenant->template->folder . '/js/app.js') }}" defer></script>

    {{-- Fonts --}}
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    {{--<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">--}}

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('templates/' . $tenant->template->folder . '/css/styles.css') }}" rel="stylesheet">
</head>
<body>

<div id="app" class="">
    <div id="main" class="container-fluid">
        <div id="container" class="row row-offcanvas row-offcanvas-left">
            <div id="content" class="col p-0 bg-grey">
                @include('templates.' . $tenant->template->folder . '.layouts.partials._nav')
                <div class="col main pt-3">
                {{--@yield('breadcrumb')--}}
                @yield('content')
                </div>
                <footer class="container-fluid">
                    <p class="text-right text-white small">©{{ Now()->year }} {{ config('app.name') }}</p>
                </footer>
            </div>
        </div>
    </div>
    {{-- /.container --}}

    @if(config('app.debug'))
        {{-- Modal --}}
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Modal</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>This is a dashboard layout for Bootstrap 4. This is an example of the Modal component which you can use to show content.
                            Any content can be placed inside the modal and it can use the Bootstrap grid classes.</p>
                        <p>
                            <a href="https://www.codeply.com/go/KrUO8QpyXP" target="_ext">Grab the code at Codeply</a>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary-outline" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- /Modal --}}
    @endif
</div>
</body>
</html>
