@extends('dashboard.layouts.app')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Library</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row mb-3">
        <div class="col-12">
            {{----}}
            <div class="card">
                <div class="card-body">
                    {!! Form::model(auth()->user(),['route' => ['dashboard.account'], 'method' => 'put']) !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {{ Form::label('name', 'Name', ['class' => "control-label"]) }}
                        {{ Form::text('name', null, ['class' => "form-control"]) }}
                        @if ($errors->has('name'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('username', 'Username', ['class' => "control-label"]) }}
                        {{ Form::text('username', null, array('class' => "form-control")) }}

                        @if ($errors->has('username'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('username') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('email', 'Email', ['class' => "control-label"]) }}
                        {{ Form::text('email', null, ['class' => "form-control"]) }}
                        @if ($errors->has('email'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>

                    {{ Form::submit(__('Send'), ['class' => 'btn btn-primary btn-block']) }}

                    {!! Form::close() !!}
                </div>

            </div>
            {{----}}
        </div>
    </div>
@endsection
