@extends('dashboard.layouts.app')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Library</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row mb-3">

        {{-- Count --}}
        <div class="col-xl-3 col-sm-6 py-2">
            <div class="card text-white bg-info h-100">
                <div class="card-body bg-info">
                    <div class="rotate">
                        <i class="fas fa-book fa-4x"></i>
                    </div>
                    <h6 class="text-uppercase">Narratives</h6>
                    <h1 class="display-4">125</h1>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 py-2">
            <div class="card text-white bg-info h-100">
                <div class="card-body bg-info">
                    <div class="rotate">
                        <i class="fas fa-tasks fa-4x"></i>
                    </div>
                    <h6 class="text-uppercase">Tasks</h6>
                    <h1 class="display-4">125</h1>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 py-2">
            <div class="card text-white bg-info h-100">
                <div class="card-body bg-info">
                    <div class="rotate">
                        <i class="fas fa-envelope fa-4x"></i>
                    </div>
                    <h6 class="text-uppercase">Messages</h6>
                    <h1 class="display-4">125</h1>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 py-2">
            <div class="card text-white bg-info h-100">
                <div class="card-body bg-info">
                    <div class="rotate">
                        <i class="fas fa-address-book fa-4x"></i>
                    </div>
                    <h6 class="text-uppercase">Contacts</h6>
                    <h1 class="display-4">125</h1>
                </div>
            </div>
        </div>
        {{-- /Counts --}}

        {{----}}
        <div class="col-xl-12 col-sm-12 py-2">
        </div>
        {{----}}
    </div>
@endsection
