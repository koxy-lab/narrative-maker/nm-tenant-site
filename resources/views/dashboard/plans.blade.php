@extends('dashboard.layouts.app')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Library</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row mb-3">
        {{----}}
        @foreach($plans as $plan)
        <div class="col-xl-4 col-sm-12 py-2">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title text-primary">{{ $plan->name }} - <span>{{ $plan->completePrice }}</span></h3>
                    <div class="card-text">{{ $plan->description }}</div>
                </div>
                <div class="card-footer">
                    @subscribed($plan->name)

                        {!! Form::open(['route' => ['dashboard.subscriptions.unsubscribe'], 'method' => 'delete']) !!}
                        <button class="btn btn-primary">{{ __('Unsubscribe') }}</button>
                        <input type="hidden" name="plan_id" value="{{ $plan->paypal_id }}">
                        {!! Form::close() !!}

                        @else

                        {!! Form::open(['route' => ['dashboard.subscriptions.subscribe'], 'method' => 'post']) !!}
                            <button class="btn btn-primary">{{ __('Subscribe') }}</button>
                            <input type="hidden" name="plan_id" value="{{ $plan->paypal_id }}">
                        {!! Form::close() !!}

                    @endsubscribed
                </div>
            </div>
            <hr>
        </div>
        @endforeach
        {{----}}
    </div>
@endsection
