<div id="sidebar" class="col-md-3 col-lg-2 sidebar-offcanvas bg-dark pl-0" role="navigation">
    <ul class="nav flex-column sticky-top pl-0 pt-2">
        <li class="nav-item"><a class="nav-link" href="{{ route('dashboard.home') }}">{{ __('Home') }}</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('dashboard.plans.index') }}">{{ __('Plans') }}</a></li>
    </ul>
</div>
<!--/col-->
