<nav id="navbar" class="navbar navbar-expand-md navbar-dark">
    <div class="flex-row d-flex">
        <button type="button" class="navbar-toggler mr-2 " data-toggle="offcanvas" title="Toggle responsive left sidebar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">{{ $tenant->name }}</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse" id="collapsingNavbar">
        {{-- Navbar left --}}
        {{--<ul class="navbar-nav">--}}
            {{--<li class="nav-item active">--}}
                {{--<a class="nav-link" href="#">Home <span class="sr-only">Home</span></a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href="//www.codeply.com">Link</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
        {{-- Navbar left --}}

        {{-- Navbar right --}}
        <ul class="navbar-nav ml-auto">
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="#myAlert" data-toggle="collapse">Alert</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="" data-target="#myModal" data-toggle="modal">About</a>--}}
            {{--</li>--}}
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                <li class="nav-item">
                    @if (Route::has('register'))
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    @endif
                </li>
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ route('dashboard.home') }}">
                            {{ __('Dashboard') }}
                        </a>

                        <a class="dropdown-item" href="{{ route('dashboard.account') }}">
                            {{ __('Account') }}
                        </a>

                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
        {{-- /Navbar right --}}
    </div>
</nav>

{{--<nav class="navbar navbar-expand-md navbar-dark bg-dark">--}}

    {{--<div class="col-xs-4 col-md-1">--}}
        {{--<a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">--}}
            {{--<span class="navbar-toggler-icon"></span>--}}
        {{--</a>--}}
    {{--</div>--}}

    {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
        {{--{{ config('app.name', 'Laravel') }}--}}
    {{--</a>--}}

    {{--<button class="col-xs-4 navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">--}}
        {{--<span class="navbar-toggler-icon"></span>--}}
    {{--</button>--}}

    {{--<div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
        {{--<!-- Left Side Of Navbar -->--}}
        {{--<ul class="navbar-nav mr-auto">--}}

        {{--</ul>--}}

        {{--<!-- Right Side Of Navbar -->--}}
        {{--<ul class="navbar-nav ml-auto">--}}
            {{--<!-- Authentication Links -->--}}
            {{--@guest--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--@if (Route::has('register'))--}}
                        {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                    {{--@endif--}}
                {{--</li>--}}
            {{--@else--}}
                {{--<li class="nav-item dropdown">--}}
                    {{--<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
                        {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
                    {{--</a>--}}

                    {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}

                        {{--<a class="dropdown-item" href="{{ route('dashboard.home') }}">--}}
                            {{--{{ __('Dashboard') }}--}}
                        {{--</a>--}}

                        {{--<a class="dropdown-item" href="{{ route('logout') }}"--}}
                           {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                            {{--{{ __('Logout') }}--}}
                        {{--</a>--}}

                        {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                            {{--@csrf--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</li>--}}
            {{--@endguest--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--</nav>--}}
