<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $tenant->name }}</title>

    {{-- Scripts --}}
    <script src="{{ asset('dashboard/js/app.js') }}" defer></script>

    {{-- Fonts --}}
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    {{--<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">--}}

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    {{-- Styles --}}
    <link href="{{ asset('dashboard/css/styles.css') }}" rel="stylesheet">
</head>
<body>

<div id="app" class="">
    <div id="main" class="container-fluid">
        <div id="container" class="row row-offcanvas row-offcanvas-left">
            @guest
            @else
                @include('dashboard.layouts.partials._aside_navbar')
            @endguest

            <div id="content" class="col p-0 bg-grey">
                @include('dashboard.layouts.partials._navbar')
                <div class="col main pt-3">
                {{--@yield('breadcrumb')--}}
                @yield('content')
                </div>
                <footer class="container-fluid">
                    <p class="text-right text-white small">©{{ Now()->year }} {{ config('app.name') }}</p>
                </footer>
            </div>
        </div>
    </div>
    {{-- /.container --}}
</div>
</body>
</html>
