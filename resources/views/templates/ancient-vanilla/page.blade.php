@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ $page->title }}</title>
    @include('components.meta', [ 'title' => $page->title, 'description' => $page->description ])
@endsection

@section('content')
    <section class="pt-4">
        <div class="row pt-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @include('templates.ancient-vanilla.components._post-header', [ 'name' => $page->title, 'description' => $page->description ])
                        @include('templates.ancient-vanilla.components._post-text', [ 'text' => $page->text ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
