<header class="post-header">
    <h1 class="card-title text-primary">{{ $name }}</h1>
    <p class="card-text">{{ $description }}</p>
</header>
