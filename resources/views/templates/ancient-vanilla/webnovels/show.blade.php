@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ $webnovel->name }} - {{ $tenant->name }}</title>
    @include('components.meta', [ 'title' => $webnovel->name .' - '. $tenant->name, 'description' => $webnovel->description ])
@endsection

@section('content')
    <section class="pt-4">
        <div class="row pt-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @include('templates.ancient-vanilla.components._post-header', [ 'name' => $webnovel->name, 'description' => $webnovel->description ])
                        @include('templates.ancient-vanilla.components._post-text', [ 'text' => $webnovel->text ])
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-4">
        <div class="row">
            @foreach($webnovel->chapters()->get() as $chapter)
                <div class="col-4 my-2">
                    @include('templates.' . $tenant->template->folder . '..components._chapter-card', $chapter)
                </div>
            @endforeach
        </div>
    </section>
@endsection
