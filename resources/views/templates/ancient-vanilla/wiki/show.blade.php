@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ $sheet->title }} - {{ $tenant->name }}</title>
    @include('components.meta', [ 'title' => $sheet->title .' - '. $tenant->name, 'description' => $sheet->description ])
@endsection

@section('jumbotron')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <div class="float-right d-flex flex-column">
                <span>{{ $sheet->created_at->diffForHumans() }}</span>
            </div>

            <h1 class="display-4">{{ $sheet->title }}</h1>
            <p class="lead">{{ $sheet->description }}</p>
        </div>
    </div>
@endsection

@section('content')
    <section class="pt-4">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @include('templates.ancient-vanilla.components._post-header', [ 'name' => $sheet->title, 'description' => $sheet->description ])
                        @include('templates.ancient-vanilla.components._post-text', [ 'text' => $sheet->text ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
