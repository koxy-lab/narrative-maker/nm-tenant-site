@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ __('Wiki') }} - {{ $tenant->name }}</title>
@endsection

@section('jumbotron')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">{{ __('Wiki') }}</h1>
        </div>
    </div>
@endsection

@section('content')
    <section class="py-4">
        <div class="row">
            @foreach($taxonomies as $taxonomy)
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $taxonomy->name }}</h5>
                            <p class="card-text">{{ $taxonomy->description }}</p>
                        </div>

                        @if($taxonomy->children()->count())
                            <div class="card-footer">
                                <a href="{{ route('wiki.show', $taxonomy->slug) }}" class="btn btn-primary">
                                    {{ __('View more') }}
                                </a>
                                <button class="btn btn-primary"
                                        type="button"
                                        data-toggle="collapse"
                                        data-target="#collapse{{ $taxonomy->id }}"
                                        aria-expanded="false"
                                        aria-controls="collapse{{ $taxonomy->id }}">
                                    {{ __('Subcategories') }}
                                </button>
                            </div>
                            <div class="collapse" id="collapse{{ $taxonomy->id }}">
                                <ul class="list-group">
                                    @foreach($taxonomy->children as $child)
                                        <li class="list-group-item">
                                            <a href="{{ route('wiki.show', $child->slug) }}" class="btn-link">{{ $child->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @else
                            <div class="card-footer">
                                <a href="{{ route('wiki.show', $taxonomy->slug) }}" class="btn btn-primary">
                                    {{ __('View more') }}
                                </a>
                                <button class="btn btn-primary disabled"
                                        type="button">
                                    {{ __('Subcategories') }}
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection

@section('content')
    @foreach($taxonomies as $taxonomy)
        <section class="py-4">
            <header class="text-center text-primary mb-4">
                <h3>{{ $taxonomy->name }}</h3>
                <p class="mb-1">
                    {{ $taxonomy->description }}
                </p>
            </header>
            <div class="row justify-content-center">
                @foreach($taxonomy->sheets()->limit(3)->get() as $sheet)
                <div class="col-sm-6">
                    <div class="card text-center">
                        <div class="card-header">
                            <h5 class="card-title"><a href="{{ route('wiki.show', [$sheet->slug]) }}">{{ $sheet->title }}</a></h5>
                        </div>
                        <div class="card-body">
                            <p class="card-text">{{ $sheet->description }}</p>
                        </div>
                        <div class="card-footer text-muted">
                            <a href="{{ route('wiki.show', [$sheet->slug]) }}" class="btn btn-primary">{{ __('View more') }}</a>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-12 text-center mt-4">
                    <a href="{{ route('wiki.show', $taxonomy->slug) }}" class="btn btn-primary">
                        {{ __('View all') }}
                    </a>
                </div>
            </div>
        </section>
    @endforeach
@endsection
