@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('jumbotron')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb p-0 bg-transparent">
                    <li class="breadcrumb-item"><a href="/">{{ __('Home') }}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('wiki.index') }}">{{ __('Wiki') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ $taxonomy->name }}</li>
                </ol>
            </nav>

            <div class="float-right d-flex flex-column">
                <span>{{ $taxonomy->created_at->diffForHumans() }}</span>
            </div>

            <h1 class="display-4">{{ $taxonomy->name }}</h1>
            <p class="lead">{{ $taxonomy->description }}</p>
        </div>
    </div>
@endsection

@section('content')
    <section class="py-4">
        <div class="row">
            @if($taxonomy->children()->count())
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header">
                        {{ __('Subcategories') }}
                    </div>
                    <ul class="list-group">
                        @foreach($taxonomy->children as $child)
                            <li class="list-group-item">
                                <a href="{{ route('wiki.show', $child->slug) }}" class="btn-link">{{ $child->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif

            <div class="{{ $taxonomy->children()->count() ? 'col-sm-8' : 'col-sm-12'}}">
                <div class="row">
                    @if($taxonomy->sheets()->count())
                        @foreach($taxonomy->sheets as $child)
                            <div class="{{ $taxonomy->children()->count() ? 'col-sm-6' : 'col-sm-4'}}">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card--title">{{ $child->title }}</h5>
                                        <p class="card-text">{{ $child->description }}</p>
                                    </div>
                                    <div class="card-footer">
                                        <a href="{{ route('wiki.show', $child->slug) }}" class="btn btn-primary">{{ __('View more') }}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
