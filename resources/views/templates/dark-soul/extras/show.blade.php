@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ $extra->title }} - {{ $tenant->name }}</title>
    @include('components.meta', [ 'title' => $extra->title .' - '. $tenant->name, 'description' => $extra->description ])
@endsection

@section('content')
    <section class="pt-4">
        <div class="row pt-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @include('templates.ancient-vanilla.components._post-header', [ 'name' => $extra->title, 'description' => $extra->description ])
                        @include('templates.ancient-vanilla.components._post-text', [ 'text' => $extra->text ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
