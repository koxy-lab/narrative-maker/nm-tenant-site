<div class="card">
    <div class="card-header">
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">{{ $chapter->name }}</h5>
            <small>{{ $chapter->created_at->diffForHumans() }}</small>
        </div>
        <small><a href="#">{{ $chapter->webnovel->name }}</a></small>
    </div>
    <article class="card-body">
        <p class="mb-1">{{ $chapter->description }}</p>
    </article>
    <div class="card-footer">
        <a href="{{ route('webnovels.chapters.show', [$chapter->webnovel->slug,$chapter->slug]) }}" class="btn btn-block btn-primary">{{ __('More') }}</a>
    </div>
</div>
