<div class="col-2 collapse d-md-flex bg-faded pt-2 h-100" id="sidebar">
    <ul class="nav flex-column">
        <li class="nav-item">

            @tenant
            {{-- TODO: add tenant name --}}
            @endtenant

            <a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">
                {{ optional(request()->tenant())->name ?: 'Narratives' }}
            </a>

            <div class="collapse" id="submenu1" aria-expanded="false">
                <ul class="flex-column pl-2 nav">

                    @if(isset($narratives) && $narratives->count())
                        @foreach($narratives as $narrative)
                            <li class="nav-item">
                                <a class="nav-link py-0" href="{{ route('tenant.switch', $narrative->uuid) }}">{{ $narrative->name }}</a>
                            </li>
                        @endforeach

                        <div class="dropdown-divider"></div>
                    @endif

                    <li class="nav-item">
                        <a class="nav-link py-0" href="{{ route('narratives.create') }}">
                            <i class="fa fa-fw fa-compass"></i> New narrative
                        </a>
                    </li>

                </ul>
            </div>
        </li>

        @tenant
        <li class="nav-item"><a class="nav-link" href="{{ route('projects.index') }}">Projects</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('pages.index') }}">Pages</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('taxonomies.index') }}">Taxonomies</a></li>
        @endtenant
    </ul>
</div>
