<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('title')
        <title>{{ $tenant->name }}</title>

    <!-- Scripts -->
    <script src="{{ asset('templates/'. $tenant->template->folder .'/js/app.js') }}" defer></script>

    {{-- Fonts --}}
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('templates/'. $tenant->template->folder .'/css/styles.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
</head>
<body>
<div id="app">
    @include('templates/' . $tenant->template->folder . '.layouts.partials._nav')

    @yield('jumbotron')
    <main class="main my-5">
        <div class="container h-100">
            @yield('content')
        </div>
    </main>

    @include('templates/' . $tenant->template->folder . '.layouts.partials._footer')
</div>
</body>
</html>
