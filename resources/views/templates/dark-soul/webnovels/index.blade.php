@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ __('Webnovels') }} - {{ $tenant->name }}</title>
@endsection

@section('jumbotron')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">{{ __('Webnovels') }}</h1>
        </div>
    </div>
@endsection

@section('content')
    <section class="py-4">
        <div class="row">
            @foreach($webnovels as $novel)
                <div class="col-4 my-2">
                    @include('templates.' . $tenant->template->folder . '..components._novel-card', $novel)
                </div>
            @endforeach
        </div>
    </section>
@endsection
