@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ $project->name }} - {{ $tenant->name }}</title>
    @include('components.meta', [ 'title' => $project->name .' - '. $tenant->name, 'description' => $project->description ])
@endsection

@section('content')
    <section class="pt-4">
        <div class="row pt-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @include('templates.ancient-vanilla.components._post-header', [ 'name' => $project->title, 'description' => $project->description ])
                        @include('templates.ancient-vanilla.components._post-text', [ 'text' => $project->text ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
