@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ __('Projects') }} - {{ $tenant->name }}</title>
@endsection

@section('jumbotron')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">{{ __('Projects') }}</h1>
        </div>
    </div>
@endsection

@section('content')
    <section class="py-4">
        <div class="row">
            @foreach($projects as $project)
                <div class="col-4">
                    @include('templates.' . $tenant->template->folder . '..components._project-card', $project)
                </div>
            @endforeach
        </div>
    </section>
@endsection
