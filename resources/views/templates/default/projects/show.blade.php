@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ $project->name }} - {{ $tenant->name }}</title>
    @include('components.meta', [ 'title' => $project->name .' - '. $tenant->name, 'description' => $project->description ])
@endsection

@section('content')
    <section class="pt-4">
        <div class="row pt-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <header class="text-center">
                            <h1 class="card-title text-primary">{{ $project->name }}</h1>
                            <p class="card-text">{{ $project->description }}</p>
                        </header>
                        <article class="mt-2 post-text">
                            {!! $project->text !!}
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
