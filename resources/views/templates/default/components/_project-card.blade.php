<div class="card">
    <article class="card-body">
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">{{ $project->name }}</h5>
            <small>{{ $project->created_at->diffForHumans() }}</small>
        </div>
        <p class="mb-1">{{ $project->description }}</p>
    </article>
    <div class="card-footer">
        <a href="{{ route('projects.show', $project->slug) }}" class="btn btn-block btn-primary">{{ __('More') }}</a>
    </div>
</div>
