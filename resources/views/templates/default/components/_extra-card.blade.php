<div class="card">
    <article class="card-body">
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">{{ $extra->title }}</h5>
            <small>{{ $extra->created_at->diffForHumans() }}</small>
        </div>
        <p class="mb-1">{{ $extra->description }}</p>
    </article>
    <div class="card-footer">
        <a href="{{ route('extras.show', $extra->slug) }}" class="btn btn-block btn-primary">{{ __('More') }}</a>
    </div>
</div>
