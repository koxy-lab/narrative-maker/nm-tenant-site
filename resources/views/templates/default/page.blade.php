@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('content')
    <section class="pt-4">
        <div class="row pt-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-body text-center">
                        <h1 class="card-title text-primary">{{ $page->title }}</h1>
                        <p class="card-text post-text">{!! $page->text !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
