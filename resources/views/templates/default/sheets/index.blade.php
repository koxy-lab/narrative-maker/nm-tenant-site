@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('jumbotron')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">{{ __('Wiki') }}</h1>
        </div>
    </div>
@endsection

@section('content')
    <section class="py-4">
        <div class="row">
            @foreach($sheets as $sheet)
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">{{ $sheet->name }}</h4>
                            <p class="card-text">{{ $sheet->description }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection


@section('content')

    <section class="pt-5">
        <h1>{{ $taxonomy }}</h1>
        <div class="row pt-2">
            @foreach($sheets as $sheet)
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">{{ $sheet->name }}</h4>
                            <p class="card-text">{{ $sheet->description }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>

@endsection
