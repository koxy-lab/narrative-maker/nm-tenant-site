<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('title')
        <title>{{ $tenant->name }}</title>

    <!-- Scripts -->
    <script src="{{ asset('templates/'. $tenant->template->folder .'/js/app.js') }}" defer></script>

    {{-- Fonts --}}
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('templates/'. $tenant->template->folder .'/css/styles.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
</head>
<body>
<div id="app">
    {{--{{ 'templates/' . $tenant->template->folder }}--}}
    @include('templates.' . $tenant->template->folder . '..layouts.partials._nav')

    @yield('jumbotron')
    <main class="my-5">
        <div class="container h-100">
            @yield('content')
        </div>
    </main>

    {{-- Footer --}}
    <section id="footer" class="py-5">
        <div class="container">
            @debug
                <div class="row text-center text-xs-center text-sm-left text-md-left">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <h5>Quick links</h5>
                        <ul class="list-unstyled quick-links">
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Home</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>About</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Get Started</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Videos</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <h5>Quick links</h5>
                        <ul class="list-unstyled quick-links">
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Home</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>About</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Get Started</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Videos</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <h5>Quick links</h5>
                        <ul class="list-unstyled quick-links">
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Home</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>About</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Get Started</a></li>
                            <li><a href="#"><i class="fa fa-angle-double-right"></i>Videos</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                        <ul class="list-unstyled list-inline social text-center">
                            <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-facebook"></i></a></li>
                            {{--<li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-twitter"></i></a></li>--}}
                            {{--<li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-instagram"></i></a></li>--}}
                            {{--<li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>--}}
                        </ul>
                    </div>
                    </hr>
                </div>
            @enddebug
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                    <p class="h6">&copy All right Reversed.<a class="text-copyright ml-2" href="#" target="_blank">{{ $tenant->users[0]->name }}</a></p>
                    <small><a class="text-copyright ml-2" href="//narrativemaker.com" target="_blank">Narrative Maker</a></small>
                </div>
                </hr>
            </div>
        </div>
    </section>
    {{-- ./Footer --}}

</div>
</body>
</html>
