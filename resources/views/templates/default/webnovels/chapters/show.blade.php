@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ $chapter->name }} - {{ $tenant->name }}</title>
    @include('components.meta', [ 'title' => $chapter->name .' - '. $tenant->name, 'description' => $chapter->description ])
@endsection

@section('content')
    <section class="pt-4">
        <div class="row pt-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <header class="text-center">
                            <h1 class="card-title text-primary">{{ $chapter->name }}</h1>
                            <p class="card-text">{{ $chapter->description }}</p>
                        </header>
                        <article class="mt-2 post-text">
                            {!! $chapter->text !!}
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
