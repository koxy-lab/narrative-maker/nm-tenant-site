@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ $tenant->name }}</title>
    @include('components.meta', [ 'title' => $tenant->name, 'description' => $tenant->description ])
@endsection

@section('content')

    <section class="pt-4">
        <div class="row pt-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <header class="text-center">
                            <h1 class="card-title text-primary">{{ $tenant->name }}</h1>
                            <p class="card-text">{{ $tenant->description }}</p>
                        </header>
                        <article class="post-text mt-2">
                            {!! $tenant->text !!}
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @isset($extras)
    <section class="pt-5">
        <header class="text-center text-primary">
            <h3>{{ __('Extras') }}</h3>
        </header>
        <div class="row pt-2">
            @foreach($extras as $extra)
                <div class="col-4">
                    @include('templates.' . $tenant->template->folder . '..components._extra-card', $extra)
                </div>
            @endforeach
        </div>
    </section>
    @endisset

    @isset($chapters)
        <section class="pt-5">
            <header class="text-center text-primary">
                <h3>{{ __('Last chapters') }}</h3>
            </header>
            <div class="row pt-2">
                @foreach($chapters as $chapter)
                    <div class="col-4">
                        @include('templates.' . $tenant->template->folder . '..components._chapter-card', $chapter)
                        {{--@include('templates.' . $tenant->template->folder . '..components._novel-card', $novel)--}}
                    </div>
                @endforeach
            </div>
        </section>
    @endisset

    {{--@isset($projects)--}}
    {{--<section class="pt-5">--}}
        {{--<header class="text-center text-primary">--}}
            {{--<h3>{{ __('Products') }}</h3>--}}
        {{--</header>--}}
        {{--<div class="row pt-2">--}}
            {{--@foreach($projects as $project)--}}
                {{--<div class="col-4">--}}
                    {{--@include('templates.' . $tenant->template->folder . '..components._project-card', $project)--}}
                {{--</div>--}}
            {{--@endforeach--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--@endisset--}}

@endsection
