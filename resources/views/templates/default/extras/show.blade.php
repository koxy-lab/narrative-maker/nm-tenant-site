@extends('templates.' . $tenant->template->folder . '.layouts.default')

@section('title')
    <title>{{ $extra->title }} - {{ $tenant->name }}</title>
    @include('components.meta', [ 'title' => $extra->title .' - '. $tenant->name, 'description' => $extra->description ])
@endsection

@section('content')
    <section class="pt-4">
        <div class="row pt-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <header class="text-center">
                            <h1 class="card-title text-primary">{{ $extra->title }}</h1>
                            <p class="card-text">{{ $extra->description }}</p>
                        </header>
                        <article class="mt-2 post-text">
                            {!! $extra->text !!}
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
